var express = require('express');
var send = require('./src/send');
var receive = require('./src/receive');
var app = express();

app.get('/send/:message',function(req,res){
    const message = req.params.message;
    send(message);
    res.send("SEND message successfully")
});


app.get('/receive',function(req,res){
    function displayImage(msg)
    {
        res.send(msg);
    }
    receive(displayImage);
});

app.listen(4500);