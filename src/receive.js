var amqp = require('amqplib/callback_api');

function receive(sendToOther)
{
    amqp.connect('amqp://localhost',function(err,connection){
    if(err)
    {
        throw err;
    }
    connection.createChannel(function(err1,channel){ 
        if(err1)
        {
            throw err1;
        }
        var queue = "hello";
        channel.assertQueue(queue,{
            durable:true
        });

        channel.consume(queue,function(msg){
            console.log("YOUR MESSAGE::::::::::"+msg.content.toString());
            sendToOther(msg.content.toString());
            //sendToOther = msg.content.toString();
        },{
            noAck:true
        });   
    });

});
}

module.exports = receive;