var amqp = require('amqplib/callback_api');


function sendMessage(msg)
{
    amqp.connect('amqp://localhost',function(err,connection){
    if(err)
    {
        throw err;
    }
    connection.createChannel(function(err1,channel){
        if(err1)
        {
            throw err1;
        }
        var queue = "hello";
        //var msg = msg;
        channel.assertQueue(queue,{
            durable:true
        });
        channel.sendToQueue(queue,Buffer.from(msg));
        console.log("MESSAGE is send ::::"+msg);
    });
    setTimeout(function(){
        connection.close();
        //process.exit(0);
    },500);
});
}

module.exports = sendMessage;

